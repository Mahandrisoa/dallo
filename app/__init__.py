from flask import Flask

from flask_migrate import Migrate
from flask_jwt_extended import JWTManager

from app.database import db
from config import DevelopmentConfig


migrate = Migrate()

def create_app():
    app = Flask(__name__)
    app.config.from_object(DevelopmentConfig)

    db.init_app(app)
    jwt = JWTManager(app)
    migrate.init_app(app, db)

    # Register the routes blueprint
    from app.routes.routes import routes
    app.register_blueprint(routes)

    return app