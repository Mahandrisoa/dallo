from flask import Blueprint   

from flask_jwt_extended import jwt_required

from app.routes.users import users_routes
from app.routes.flights import flights_routes

routes = Blueprint('routes', __name__)

routes.register_blueprint(users_routes)
routes.register_blueprint(flights_routes)

@routes.route('/')
def home():
   return "hello world!"


@routes.route('/protected', methods=['GET'])
@jwt_required()
def protected_route():
   # Your protected route logic here
   return 'Protected route content'
