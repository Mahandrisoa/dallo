from flask import Blueprint, request, jsonify
from flask_jwt_extended import create_access_token, jwt_required, get_jwt_identity
from flask_bcrypt import generate_password_hash, check_password_hash

from app.models.user import User

from app.database import db

users_routes = Blueprint('users_routes', __name__)

@users_routes.route('/login', methods=['POST'])
def login():
   data = request.get_json()

   # Retrieve username and password from the request data
   username = data.get('username')
   password = data.get('password')

   # Perform user authentication logic (validate credentials against User model)
   user = User.query.filter_by(username=username).first()
   if user and user.check_password(password):
      # If authentication is successful, generate an access token
      access_token = create_access_token(identity=user.username)
      # Return the access token to the client
      return jsonify({'access_token': access_token}), 200
   else:
      # If authentication fails, return an error message
      return jsonify({'message': 'Invalid username or password'}), 401


# User registration endpoint
@users_routes.route('/register', methods=['POST'])
def register_user():
   data = request.get_json()
   print(data)

   # Extract user details from the request data
   username = data.get('username')
   password = data.get('password')
   email = data.get('email')

   # Check if the username or email already exists in the database
   if User.query.filter_by(username=username).first():
      return jsonify({'message': 'Username already exists'}), 400
   if User.query.filter_by(email=email).first():
      return jsonify({'message': 'Email already exists'}), 400

   # Create a new user object
   new_user = User(username=username, email=email)
   new_user.set_password = password

   # Add the new user to the database
   db.session.add(new_user)
   db.session.commit()

   return jsonify({'message': 'User registered successfully'}), 201


@users_routes.route('/users/profile', methods=['GET'])
@jwt_required()
def get_profile():
   # Retrieve the current user's identity from the JWT token
   username = get_jwt_identity()

   # Retrieve the current user from the database
   user = User.query.filter_by(username=username).first()

   if not user:
      return jsonify({'message': 'User not found'}), 404

   # Serialize the user object
   serialized_user = {
      'id': user.id,
      'username': user.username,
      'email': user.email
   }

   # Return the user profile
   return jsonify(serialized_user), 200


@users_routes.route('/users/password', methods=['PUT'])
@jwt_required()
def update_password():
   # Retrieve the current user's identity from the JWT token
   username = get_jwt_identity()

   # Retrieve the new password from the request data
   new_password = request.json.get('new_password')

   # Retrieve the current user from the database
   user = User.query.filter_by(username=username).first()

   if not user:
      return jsonify({'message': 'User not found'}), 404

   # Update the user's password
   user.set_password(new_password)
   db.session.commit()

   return jsonify({'message': 'Password updated successfully'}), 200


@users_routes.route('/profile', methods=['PUT'])
@jwt_required()
def update_profile():
   current_username = get_jwt_identity()
   user = User.query.filter_by(username=current_username).first()

   if not user:
      return jsonify({'message': 'User not found'}), 404

   data = request.get_json()

   # Update the user's profile
   user.name = data.get('name', user.first_name)
   user.email = data.get('email', user.email)

   db.session.commit()

   return jsonify({'message': 'Profile updated successfully'}), 200


@users_routes.route('/users/<username>', methods=['GET'])
@jwt_required()
def get_user_details(username):
   with db.session.begin():
      user = User.query.filter_by(username=username).first()

   if user:
      # Serialize the user object
      serialized_user = {
         'id': user.id,
         'username': user.username,
         'email': user.email
      }
      # Return the user details
      return jsonify(serialized_user), 200
   else:
      return jsonify({'message': 'User not found'}), 404