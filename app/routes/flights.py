from flask import Blueprint, request, jsonify, current_app
from flask_jwt_extended import jwt_required

from app.database import db

from app.models.flight import Flight

flights_routes = Blueprint('flights_routes', __name__)


@flights_routes.route('/flights', methods=['GET'])
def get_all_flights():
    with current_app.app_context():
      flights = Flight.query.all()
      return jsonify([flight.serialize() for flight in flights])


@flights_routes.route('/flights', methods=['POST'])
@jwt_required()
def create_flight():
    data = request.get_json()
    flight = Flight(
        origin=data.get('origin'),
        destination=data.get('destination'),
        departure_date=data.get('departure_date'),
        arrival_date=data.get('arrival_date')
    )
    db.session.add(flight)
    db.session.commit()
    return jsonify(flight.serialize()), 201


@flights_routes.route('/flights/<int:flight_id>', methods=['PUT'])
@jwt_required()
def update_flight(flight_id):
    data = request.get_json()
    flight = Flight.query.get(flight_id)
    if not flight:
        return jsonify({'error': 'Flight not found'}), 404
    flight.origin = data.get('origin', flight.origin)
    flight.destination = data.get('destination', flight.destination)
    flight.departure_date = data.get('departure_date', flight.departure_date)
    flight.arrival_date = data.get('arrival_date', flight.arrival_date)
    db.session.commit()
    return jsonify(flight.serialize())


@flights_routes.route('/flights/<int:flight_id>', methods=['DELETE'])
def delete_flight(flight_id):
    flight = Flight.query.get(flight_id)
    if not flight:
        return jsonify({'error': 'Flight not found'}), 404
    db.session.delete(flight)
    db.session.commit()
    return jsonify({'message': 'Flight deleted'})