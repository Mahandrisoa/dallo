from app.database import db

class FlightRequest(db.Model):
    __tablename__ = 'flight_requests'

    id = db.Column(db.Integer, primary_key=True)
    passenger_count = db.Column(db.Integer, nullable=False)
    
    flight_id = db.Column(db.Integer, db.ForeignKey('flights.id'))
    flight = db.relationship('Flight', backref='flight_requests')

    def __init__(self, origin, destination, departure_date, return_date, passenger_count):
        self.origin = origin
        self.passenger_count = passenger_count

    def serialize(self):
        return {
            'id': self.id,
            'passenger_count': self.passenger_count,
            'flight_id': self.flight_id
        }
