from app.database import db

class Flight(db.Model):
    id = db.Column(db.Integer, primary_key=True)
    origin = db.Column(db.String(50))
    destination = db.Column(db.String(50))
    departure_date = db.Column(db.DateTime)
    arrival_date = db.Column(db.DateTime)
    total_passenger_count = db.Column(db.Integer, nullable=False)

    def serialize(self):
        return {
            'id': self.id,
            'origin': self.origin,
            'destination': self.destination,
            'departure_date': self.departure_date.isoformat(),
            'arrival_date': self.arrival_date.isoformat(),
            'total_passenger_count': self.total_passenger_count,
        }
