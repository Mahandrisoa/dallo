from app.models.user import User
from app import db
import datetime

def seed_users():
    users = [
        User(username='user1', email='user1@example.com'),
        User(username='user2', email='user2@example.com'),
        User(username='user3', email='user3@example.com'),
        # Add more user instances as needed
    ]

    for user in users:
        user.set_password('testpassword')  # Set a default password for each user
        db.session.add(user)
    
    db.session.commit()

    print('User seed completed.')