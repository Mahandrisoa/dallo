from app.models.flight import Flight
from app import db
import datetime

def seed_flights():
    flights = [
        Flight(origin='New York', destination='London', 
               departure_date=datetime.datetime(2023, 5, 1, 8, 0), arrival_date=datetime.datetime(2023, 5, 1, 10, 30), total_passenger_count=100),
        Flight(origin='London', destination='New York', 
               departure_date=datetime.datetime(2023, 5, 1, 16, 0), arrival_date=datetime.datetime(2023, 5, 1, 19, 0), total_passenger_count=100),
        Flight(origin='Paris', destination='Tokyo', 
               departure_date=datetime.datetime(2023, 5, 2, 12, 0), arrival_date=datetime.datetime(2023, 5, 3, 4, 30), total_passenger_count=100),
        Flight(origin='Tokyo', destination='Paris', 
               departure_date=datetime.datetime(2023, 5, 5, 10, 0), arrival_date=datetime.datetime(2023, 5, 5, 23, 0), total_passenger_count=100),
        Flight(origin='New York', destination='Paris', 
               departure_date=datetime.datetime(2023, 5, 6, 7, 0), arrival_date=datetime.datetime(2023, 5, 6, 13, 30), total_passenger_count=100),
        Flight(origin='Paris', destination='New York', 
               departure_date=datetime.datetime(2023, 5, 7, 12, 0), arrival_date=datetime.datetime(2023, 5, 7, 16, 0), total_passenger_count=100)
    ]
    for flight in flights:
        db.session.add(flight)
    db.session.commit()
