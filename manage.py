from flask_script import Manager
from app import create_app
from app import db
from app.seeds.flight_seeds import seed_flights
from app.seeds.user_seeds import seed_users


app = create_app()
manager = Manager(app)

@manager.command
def create_tables():
    db.create_all()

@manager.command
def drop_tables():
    db.drop_all()

@manager.command
def seed():
    seed_flights()
    seed_users()

@manager.command
def runserver():
    app.run()

if __name__ == '__main__':
    manager.run()
