#!/bin/bash

# Get the ID of the running container
CONTAINER_ID=$(docker ps -qf "ancestor=dalloserver")

# Stop the container
if [ -n "$CONTAINER_ID" ]; then
    docker stop $CONTAINER_ID
fi
