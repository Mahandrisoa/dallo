FROM python:3.9-slim-buster

# set work directory
WORKDIR /app

# install dependencies
RUN apt-get update && apt-get install -y --no-install-recommends \
    build-essential \
    default-libmysqlclient-dev \
    && rm -rf /var/lib/apt/lists/*

# copy project
COPY . .

# install project dependencies
RUN pip install --no-cache-dir -r requirements.txt

# expose port
EXPOSE 5000

# set environment variables
ENV FLASK_APP=manage.py
ENV FLASK_ENV=development

# initialize database
RUN flask db init
RUN flask db migrate
RUN flask db upgrade
RUN python manage.py seed

# start app
CMD ["flask", "run", "--host=0.0.0.0"]
