#!/bin/bash

# Set the endpoint URL
URL="http://localhost:5000/register"

# Get the username, email, and password from command-line arguments
USERNAME="$1"
EMAIL="$2"
PASSWORD="$3"

# Check if all arguments are provided
if [ -z "$USERNAME" ] || [ -z "$EMAIL" ] || [ -z "$PASSWORD" ]; then
    echo "Usage: ./register.sh <username> <email> <password>"
    exit 1
fi

# Create a JSON payload
DATA=$(cat <<EOF
{
  "username": "$USERNAME",
  "email": "$EMAIL",
  "password": "$PASSWORD"
}
EOF
)

# Perform the POST request with the user registration data
curl -X POST \
  -H "Content-Type: application/json" \
  -d "$DATA" \
  "$URL"
