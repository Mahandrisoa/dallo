#!/bin/bash

# Set the endpoint URL
URL="http://localhost:5000/protected"

# Set the access token
TOKEN="$1"

# Check if the username or password is missing
if [ -z "$TOKEN" ]; then
  echo "Usage: $0 <token>"
  exit 1
fi

# Perform the GET request with the access token
curl -X GET \
  -H "Authorization: Bearer $TOKEN" \
  "$URL"