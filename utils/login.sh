#!/bin/bash

# Set the endpoint URL
URL="http://localhost:5000/login"

# Get the username and password from command-line arguments
USERNAME="$1"
PASSWORD="$2"

# Check if the username or password is missing
if [ -z "$USERNAME" ] || [ -z "$PASSWORD" ]; then
  echo "Usage: $0 <username> <password>"
  exit 1
fi

# Create the JSON data
JSON_DATA="{\"username\":\"$USERNAME\",\"password\":\"$PASSWORD\"}"

# Perform the POST request
curl -X POST \
  -H "Content-Type: application/json" \
  -d "$JSON_DATA" \
  "$URL"
