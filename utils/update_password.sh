#!/bin/bash

# Usage: ./update_password.sh <token> <new_password>

BASE_URL="http://localhost:5000"
API_ENDPOINT="/users/password"
TOKEN="$1"
NEW_PASSWORD="$2"

# Make the API request
curl -X PUT \
  -H "Content-Type: application/json" \
  -H "Authorization: Bearer $TOKEN" \
  -d "{\"new_password\": \"$NEW_PASSWORD\"}" \
  "$BASE_URL$API_ENDPOINT"
